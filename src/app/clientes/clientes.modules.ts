import { NgModule } from "@angular/core";
import { ClientesListComponent } from "./components/clientes-list/clientes-list.component";
import { ClientesFormComponent } from "./containers/clientes-form/clientes-form.component";
import { ClientesListagemComponent } from "./containers/clientes-listagem/clientes-listagem.component";

@NgModule({
    declarations: [
        ClientesListComponent,
        ClientesListagemComponent,
        ClientesFormComponent
    ],
    imports: []
})
export class ClientesModule {

}