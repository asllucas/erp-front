import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClientesListagemComponent } from "./containers/clientes-listagem/clientes-listagem.component";

const routes: Routes = [
    {
      path: '',
      redirectTo: '/clientes',
      component: ClientesListagemComponent
    },
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientesRoutingModule{

}