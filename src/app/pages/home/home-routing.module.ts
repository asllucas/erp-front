import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClientesListagemComponent } from "src/app/clientes/containers/clientes-listagem/clientes-listagem.component";
import { WelcomeComponent } from "../welcome/welcome.component";
import { HomeComponent } from "./home.component";

const routes: Routes = [
    { 
        path: '', 
        component: HomeComponent,
        children:[
            {
                path:'',
                redirectTo: 'welcome'
                
            },
            {
                path:'welcome',
                component: WelcomeComponent,
            },
            {
                path: '',
                redirectTo: 'clientes'
            },
            {
                path: 'clientes',
                component: ClientesListagemComponent
            }
        ] 
     },
];


@NgModule({

    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
    
})
export class HomeRoutingModule{

}