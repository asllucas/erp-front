import { CommonModule, registerLocaleData } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzMenuModule } from "ng-zorro-antd/menu";
import { IconsProviderModule } from "src/app/pages/icons-provider.module";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from './home.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { pt_BR } from 'ng-zorro-antd/i18n';
import pt from '@angular/common/locales/pt';

registerLocaleData(pt);

@NgModule({
    declarations:[HomeComponent],
    imports: [
        CommonModule,
        FormsModule,
        NzLayoutModule, 
        NzMenuModule, 
        IconsProviderModule,
        HomeRoutingModule
    ],
    exports:[
        HomeComponent
    ],
    providers: [{ provide: NZ_I18N, useValue: pt_BR }],
})

export class HomeModule{

}