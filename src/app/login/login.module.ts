import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ZorroModule } from "../pages/zorro.module";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";

@NgModule({
    declarations:[LoginComponent],
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ZorroModule,
        LoginRoutingModule
    ]
})
export class LoginModule{

}